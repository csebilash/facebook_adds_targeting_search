var express = require('express');
var curl = require('curlrequest');
var mysql = require("mysql");
var app = express();

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "script"
});

con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('MySql Connection established');
});



app.get('/', function (req, res) {

  console.log("---------- Facebook Adds API Script Running ----------");
  var fbName = [];
  var search_class = ['household_composition','life_events','politics','industries','income','net_worth','home_type','home_ownership','ethnic_affinity','generation','household_composition','moms','office_type','family_statuses','behaviors'];

  for(var i=0;i<search_class.length;i++){

    var token ='Facebook Access Token';

    var url = "https://graph.facebook.com/v2.6/search?type=adTargetingCategory&class="+search_class[i]+"&access_token="+token;

    (function(currentClass){
      curl.request({ url: url, pretend: false }, function (err, response) {
        if(err)
          return err;
        else{
          var output = JSON.parse(response);
          if(output.data){
            output.data.forEach(function (value) {
              var data = {name : value.name, class: currentClass, type: currentClass === 'behaviors'?'Behaviour':'Demography',audience_size:value.audience_size};
              con.query('INSERT INTO fbadds SET ?', data, function(err,result){
                if(err)
                    throw err;

                console.log("Inserted Id ="+result.insertId+" & Name ="+value.name);
              });
            });
          }

        }

      });

    })(search_class[i])


  }


});

// con.end(function(err) {
//   console.log("MySql Connection Ended")
// });

app.listen(3000, function () {
  console.log('Script is running on port 3000!');
});
